﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NLog;
using PPProviderImport.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Exception = System.Exception;

namespace PPProviderImport
{
    public static class Providers
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();
        private static readonly string To = ConfigurationManager.AppSettings["recipient"];
        private static readonly string From = ConfigurationManager.AppSettings["sender"];
        private const string NewLine = "<br />";

        private static readonly string StrProviderTemplateId = ConfigurationManager.AppSettings["providerTemplateID"];

        private static readonly string StrProviderParentId = ConfigurationManager.AppSettings["providerParentID"];
        private static readonly string Api = ConfigurationManager.AppSettings["api"];
        private static readonly string BaseAddress = ConfigurationManager.AppSettings["BaseAddress"];
        private static readonly string Domain = ConfigurationManager.AppSettings["domain"];
        private static readonly string Username = ConfigurationManager.AppSettings["username"];
        private static readonly string Password = ConfigurationManager.AppSettings["password"];
        private static readonly string SitecoreApi = ConfigurationManager.AppSettings["SitecoreApi"];
        private static SitecoreUser _user;
        private static List<Provider> _providers;
        public static void GetProviderData()
        {
            Logger.Info("PPProviderImport :: GetProviderData -> Initiated : " + DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss"));
            Console.WriteLine("Starting import process for PP on " + BaseAddress.ToString());
            Logger.Info("Starting import process for PP on " + BaseAddress.ToString());

            #region Get Providers from Microservice
            _providers = GetProviders();
            _user = new SitecoreUser { domain = Domain, username = Username, password = Password };
            if (_providers == null) return;
            #endregion

            try
            {
                #region Delete Providers

                DeleteProviders();
                #endregion
                #region Import Providers from API
                try
                {
                    var options = new ParallelOptions { MaxDegreeOfParallelism = 4 };
                    Parallel.ForEach(_providers.Where(x => !string.IsNullOrEmpty(x.EchoDoctorNumber)), options,
                        InsertUpdateProvider);
                    //foreach (var provider in _providers.Where(x => !string.IsNullOrEmpty(x.EchoDoctorNumber)))
                    //{
                    //    InsertUpdateProvider(provider);
                    //}
                }
                catch (Exception ex)
                {
                    var m = new Mail()
                    {
                        From = From,
                        To = To,
                        Body = "Exception occurred  " + NewLine + " ex.Message: " + ex.Message +
                               NewLine + " ex.InnerException: " +
                               ex.InnerException + NewLine + "Full Exception: " + ex,
                        Subject = "PPProviderImport Process Failed"
                    };
                    SendMail.SendMailMessage(m);
                    Console.WriteLine(ex.Message);
                    Logger.Error("PPProviderImport :: GetProviderData -> : " + ex.Message.ToString());
                    Environment.Exit(0);
                }
                #endregion
            }
            catch (Exception ex)
            {
                var m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "PPProviderImport :: Exception occurred while running Providers.GetProviderData()" +
                           NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                           ex.InnerException + NewLine + "Full Exception: " + ex,
                    Subject = "GetProviderData Process Failed"
                };

                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    Logger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);
                Logger.Error("Exception occurred while running Models.Providers.GetProviderData() (Delete Providers)" +
                             NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                             ex.InnerException + NewLine);
            }

        }
        private static void DeleteProviders()
        {
            Logger.Info("Starting delete providers.....");
            var result = string.Empty;
            // Get Children
            var userJson = JsonConvert.SerializeObject(_user);
            using (var client = new HttpClient() {BaseAddress = new Uri(SitecoreApi)})
            {
                client.DefaultRequestHeaders.Add("user", userJson);
                client.DefaultRequestHeaders.Add("url", BaseAddress);
                client.DefaultRequestHeaders.Add("id", StrProviderParentId);
                var children = client.GetAsync(SitecoreApi + "GetParentChildren");
                children.Wait();
                var allChildren = children.Result;
                if (allChildren.StatusCode == HttpStatusCode.OK)
                {
                    result = allChildren.Content.ReadAsStringAsync().Result;
                }
                else
                {
                    // unable to get children
                    Logger.Error("DeleteProviders :: Error getting Children from Microservice. ErrorCode: (" + allChildren.StatusCode + ") " + allChildren.Content);
                    return;
                }
                
            }
            
            var array = JArray.Parse(result);
            foreach (var c in array.Children())
            {
                var contentId = c.Children<JProperty>().FirstOrDefault(x => x.Name == "ItemName")?.Value.ToString();
                var lastName = c.Children<JProperty>().FirstOrDefault(x => x.Name == "LastName")?.Value
                    .ToString();
                var firstName = c.Children<JProperty>().FirstOrDefault(x => x.Name == "FirstName")?.Value
                    .ToString();
                var id = c.Children<JProperty>().FirstOrDefault(x => x.Name == "ItemID")?.Value;

                var found = _providers.Any(item => item.EchoDoctorNumber == contentId);
                if (found) continue;

                try
                {
                    //idList.Add(id);
                    using (var client = new HttpClient() {BaseAddress = new Uri(SitecoreApi)})
                    {
                        client.DefaultRequestHeaders.Add("user", JsonConvert.SerializeObject(_user));
                        client.DefaultRequestHeaders.Add("url", BaseAddress);
                        var jsonObject = JsonConvert.SerializeObject(id);
                        var content = new StringContent(
                            JsonConvert.SerializeObject(jsonObject),
                            Encoding.UTF8,
                            "application/json");
                        var response = client.PostAsync(SitecoreApi + "DeleteProvider", content);
                        response.Wait();
                        var providerResult = response.Result;
                        if (!providerResult.IsSuccessStatusCode)
                        {
                            Logger.Error("Unable to delete provider: " + firstName + " " + lastName + " " + providerResult.Content);
                        }
                        else
                        {
                            Logger.Info(firstName + " " + lastName + " Deleted");
                        }
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    Logger.Error("Exception occurred while Deleting Provider: (" + id + ") " +
                                 NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                                 ex.InnerException + NewLine);
                }
            }
            
        }
        private static List<Provider> GetProviders()
        {
            using (var client = new HttpClient())
            {
                var response = client.GetAsync(Api);
                response.Wait();
                var result = response.Result;
                if (result.IsSuccessStatusCode)
                {
                    try
                    {
                        // convert it into a list of objects 
                        return JsonConvert.DeserializeObject<List<Provider>>(result.Content.ReadAsStringAsync().Result);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                        Logger.Error("Exception occurred while getting Providers from microservice " +
                                     NewLine + " ex.Message: " + ex.Message + NewLine + " ex.InnerException: " +
                                     ex.InnerException + NewLine);
                        return null; // get out of the process. We don't want to proceed if we can't get providers
                    }
                }
                Logger.Error("Unable to get providers from API ", result.RequestMessage);
                return null;
            }
        }
        private static void InsertUpdateProvider(Provider provider)
        {
            var strNewProvider = "";
            try
            {
                strNewProvider = provider.EchoDoctorNumber.ToString();
                var array = new JObject();
                // Get Parent Item (Provider Folder)
                using (var client = new HttpClient() {BaseAddress = new Uri(SitecoreApi)})
                {
                    client.DefaultRequestHeaders.Add("user", JsonConvert.SerializeObject(_user));
                    client.DefaultRequestHeaders.Add("url", BaseAddress);
                    client.DefaultRequestHeaders.Add("id", StrProviderParentId);
                    var children = client.GetAsync(SitecoreApi + "GetItemById");
                    children.Wait();
                    var result = children.Result;

                    if (result.StatusCode == HttpStatusCode.OK)
                    {
                        var item = result.Content.ReadAsStringAsync().Result;
                        array = JObject.Parse(item);
                    }
                    else
                    {
                        Logger.Error("InsertUpdateProvider :: Error getting Item By ID. Status Code: (" + result.StatusCode + ") " + result.Content);
                        return;
                    }
                }
                
                var parentPath = array["ItemPath"].ToString();

                // Check to see if provider exists
                using (var client = new HttpClient() {BaseAddress = new Uri(SitecoreApi)})
                {
                    client.DefaultRequestHeaders.Add("user", JsonConvert.SerializeObject(_user));
                    client.DefaultRequestHeaders.Add("url", BaseAddress);
                    client.DefaultRequestHeaders.Add("parentPath", parentPath);
                    client.DefaultRequestHeaders.Add("newId", strNewProvider);
                    var itemResult = client.GetAsync(SitecoreApi + "GetItemByPath");
                    itemResult.Wait();
                    var result = itemResult.Result;
                    switch (result.StatusCode)
                    {
                        case HttpStatusCode.NotFound:
                            Logger.Info("Adding provider (" + provider.EchoDoctorNumber + ") " + 
                                        provider.FirstName + " " + provider.LastName);
                            AddProvider(provider, strNewProvider);
                            break;
                        case HttpStatusCode.OK:
                        {
                            Logger.Info("Updating provider (" + provider.EchoDoctorNumber + ") " + 
                                        provider.FirstName + " " +provider.LastName);
                            var returnItem = result.Content.ReadAsStringAsync().Result;
                            var returnObject = JObject.Parse(returnItem);
                            var returnId = returnObject["ItemID"].ToString();
                            UpdateProvider(provider, returnId);
                            break;
                        }
                        default:
                            throw new ArgumentOutOfRangeException();
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Error("PPProviderImport :: Exception in InsertUpdateProvider :: " + ex.Message);
            }
        }
        private static void AddProvider(Provider provider, string strNewProvider)
        {
            var sitecoreProvider = BuildProvider(provider);
            using (var client = new HttpClient() {BaseAddress = new Uri(SitecoreApi)})
            {
                client.DefaultRequestHeaders.Add("user", JsonConvert.SerializeObject(_user));
                client.DefaultRequestHeaders.Add("url", BaseAddress);
                var jsonObject = JsonConvert.SerializeObject(sitecoreProvider);
                var content = new StringContent(
                    JsonConvert.SerializeObject(jsonObject),
                    Encoding.UTF8,
                    "application/json");
                
                var response = client.PostAsync(SitecoreApi + "AddProvider", content);
                response.Wait();
                var result = response.Result;
                if (result.StatusCode == HttpStatusCode.OK)
                    Logger.Info("Provider (" + strNewProvider + ") " + provider.FirstName + " " +
                                provider.LastName + " Added successfully");
                else
                    Logger.Error("Error adding provider (" + strNewProvider + ") " + provider.FirstName + " " +
                                 provider.LastName + " Error: " + result.Content);
            }
        }
        private static void UpdateProvider(Provider provider, string returnId)
        {
            // post changes to that ID
            var sitecoreProvider = BuildProvider(provider);
            sitecoreProvider.ParentID = StrProviderParentId;
            
            using (var client = new HttpClient() {BaseAddress = new Uri(SitecoreApi)})
            {
                client.DefaultRequestHeaders.Add("user", JsonConvert.SerializeObject(_user));
                client.DefaultRequestHeaders.Add("url", BaseAddress);
                client.DefaultRequestHeaders.Add("existingId", returnId);
                var objectJson = JsonConvert.SerializeObject(sitecoreProvider);
                var content = new StringContent(
                    JsonConvert.SerializeObject(objectJson),
                    Encoding.UTF8,
                    "application/json");
                var response = client.PostAsync(SitecoreApi + "UpdateProvider", content);
                response.Wait();
                var result = response.Result;
                if (result.StatusCode == HttpStatusCode.OK)
                {
                    Logger.Info("Provider (" + provider.EchoDoctorNumber + ") " + provider.FirstName + " " +
                                provider.LastName + "updated successfully");
                }
                else
                {
                    Logger.Error("Error updating provider (" + provider.EchoDoctorNumber + ") " +
                                 provider.FirstName + " " + provider.LastName + " Error: " + result.Content);
                }
            }
        }
        private static SitecoreProvider BuildProvider(Provider provider)
        {
            var sitecoreProvider = new SitecoreProvider
            {
                ItemName = provider.EchoDoctorNumber.ToString(),
                TemplateID = StrProviderTemplateId,
                FirstName = provider.FirstName.ToString(),
                MiddleName = provider.MiddleName.ToString(),
                LastName = provider.LastName.ToString()
            };
            try
            {
                sitecoreProvider.CellPhoneNumber = provider.ContactCellPhone;
                sitecoreProvider.PageNumber = provider.ContactPager;
                if (provider.GenderId != null && provider.GenderId == "F")
                    sitecoreProvider.Gender = "Female";
                else if (provider.GenderId != null && provider.GenderId == "M")
                    sitecoreProvider.Gender = "Male";
                else sitecoreProvider.Gender = "";    // clear it if not provided                
                sitecoreProvider.PhoneNumber = provider.ContactPhone;
                sitecoreProvider.JobTitle = provider.EchoSuffix;
                sitecoreProvider.PhotoURL = (provider.DoctorImage != null && provider.DoctorImage.StartsWith("https://")) ? provider.DoctorImage : null;

                if (provider.LanguageMapping!=null)
                {
                    var count = 0;
                    string lang = null;
                    foreach (var language in provider.LanguageMapping)
                    {
                        if (count == 0)
                        {
                            lang = language.Text;
                        }
                        else
                        {
                            lang = lang + "|" + language.Text;
                        }
                        count++;
                    }
                    sitecoreProvider.Languages = lang;
                }

                if (provider.SpecialtyMapping!=null && provider.SpecialtyMapping.Count>0)
                {
                    // get primary
                    if (provider.SpecialtyMapping.Any(x => x.IsPrimary))
                    {
                        sitecoreProvider.PrimarySpecialty = provider.SpecialtyMapping.Where(x => x.IsPrimary).Select(x=>x.Text).First();
                    }
                    //get other
                    if (provider.SpecialtyMapping.Any(x => !x.IsPrimary))
                    {
                        sitecoreProvider.OtherSpecialties = provider.SpecialtyMapping.Where(x => !x.IsPrimary).Select(x=>x.Text).First();
                    }
                }
                else
                {
                    //log that provider didn't have any specialties
                    Logger.Info("(" + provider.EchoDoctorNumber + ") " + provider.FirstName + " " + provider.LastName + " Didn't have any specialties.");
                }
                sitecoreProvider.Suffix = provider.EchoSuffix;
                sitecoreProvider.Biography = provider.Introduction;
                sitecoreProvider.PracticeInformation = provider.LegalPracticeName;
                if (provider.HospitalMapping!=null)
                {
                    var count = 0;
                    string hospitals = null;
                    foreach (var hospital in provider.HospitalMapping.OrderBy(x=>x.OrderId))
                    {
                        if (count==0)
                        {
                            hospitals = hospital.HospitalName;
                        }
                        else
                        {
                            hospitals = hospitals + "|" + hospital.HospitalName;
                        }
                        count++;
                    }
                    sitecoreProvider.HospitalAffiliations = hospitals;
                }
                
                sitecoreProvider.IsAcceptingPatients = (!String.IsNullOrEmpty(provider.IsAcceptingNewPatients.ToString())) ? (provider.IsAcceptingNewPatients.ToString().ToLower().Equals("true")) ? "1" : "0" : "0";
                sitecoreProvider.IsBPP = (!string.IsNullOrEmpty(provider.IsBPP.ToString())) ? (provider.IsBPP.ToString().ToLower().Equals("true")) ? "1" : "0" : "0";
                if ((!string.IsNullOrEmpty(provider.IsHide.ToString()) || !string.IsNullOrEmpty(provider.IsArchived.ToString()))
                    && ((System.Convert.ToBoolean(provider.IsHide.ToString()) == true)
                    || (System.Convert.ToBoolean(provider.IsArchived.ToString()) == true)))
                    sitecoreProvider.ExcludeFromSearch = "1";
                else
                    sitecoreProvider.ExcludeFromSearch = "0";

                var strPrimLoc = string.Empty;
                var strOthLoc = string.Empty;
                var strPrimLocLatLng = string.Empty;
                var strOthLocLatLng = string.Empty;
                //var primaryLocation = provider.Locations.FirstOrDefault(x => x.IsDefaultLocation);
                //var otherLocations = provider.Locations.Where(x => x.IsDefaultLocation == false).ToList();
                var primaryLocation = provider.Locations.FirstOrDefault(x => x.type == "Primary Address") ?? provider.Locations.FirstOrDefault(x => x.IsDefaultLocation);
                //var otherLocations = provider.Locations.Where(x => x.IsDefaultLocation == false).ToList();
                var otherLocations = provider.Locations.Where(x => x.type != "Primary Address").ToList();

                // Primary Location Info
                if (primaryLocation != null)
                {
                    var fax = primaryLocation.Fax;
                    var mainPhone = primaryLocation.mainPhone;
                    var admissions = primaryLocation.admissionsPhone;

                    if (primaryLocation.Address1.Contains("'"))
                    {
                        primaryLocation.Address1 = primaryLocation.Address1.Replace("'", "");
                    }

                    if (primaryLocation.Address2.Contains("'"))
                    {
                        primaryLocation.Address2 = primaryLocation.Address2.Replace("'", "");
                    }

                    if (primaryLocation.Address3.Contains("'"))
                    {
                        primaryLocation.Address3 = primaryLocation.Address3.Replace("'", "");
                    }

                    strPrimLoc = primaryLocation.Address1 + "|" + primaryLocation.Address2 + "|" + primaryLocation.Address3 + "|" + primaryLocation.City + "|" +
                                 primaryLocation.State + "|" + primaryLocation.Zip + "|" + mainPhone + "|" + admissions + "|" + fax;
                    strPrimLocLatLng = primaryLocation.Lat + "," + primaryLocation.Lng;
                }

                var counter = 0;
                // Other Locations
                foreach (var loc in otherLocations)
                {
                    var fax = loc.Fax;
                    var mainPhone = loc.mainPhone;
                    var admissions = loc.admissionsPhone;
                    if (counter > 0)
                    {
                        strOthLoc += "^";

                        strOthLocLatLng += "|";
                    }

                    strOthLoc += loc.Address1 + "|" + loc.Address2 + "|" + loc.Address3 + "|" + loc.City + "|" +
                                 loc.State + "|" + loc.Zip + "|" + mainPhone + "|" + admissions + "|" + fax;
                       
                    strOthLocLatLng += loc.Lat + "," + loc.Lng;
                    counter++;
                }
                
                sitecoreProvider.PrimaryLocation = strPrimLoc;
                sitecoreProvider.PrimaryLocationLatLng = strPrimLocLatLng;
                sitecoreProvider.OtherLocations = strOthLoc;//(strOthLoc.EndsWith("|")) ? strOthLoc.TrimEnd('|') : strOthLoc;
                sitecoreProvider.OtherLocationLatLngs = strOthLocLatLng;//(strOthLocLatLng.EndsWith("|")) ? strOthLocLatLng.TrimEnd('|') : strOthLocLatLng;
                
                var internship = "";
                var internshipMapping = provider.EducationMapping.FirstOrDefault(x => x.DegreeText == "Internship");
                internship = internshipMapping == null ? "" : internshipMapping.InstitutionText + "|" + internshipMapping.StartYear + "|" + internshipMapping.EndYear;
                sitecoreProvider.Internship = internship;

                var residency = "";
                var residencyMapping = provider.EducationMapping.FirstOrDefault(x => x.DegreeText == "Residency");
                residency = residencyMapping == null ? "" : residencyMapping.InstitutionText + "|" + residencyMapping.StartYear + "|" + residencyMapping.EndYear;
                sitecoreProvider.Residency = residency;
            }
            catch (Exception ex)
            {
                Logger.Error("PPProviderImport :: BuildProvider :: EXCEPTION " , ex.Message);
                Console.WriteLine(ex.Message);

                Logger.Error("PPProviderImport :: BuildProvider -> Could not update item " + sitecoreProvider.ItemName + ": " + ex.ToString());
                Console.WriteLine("PPProviderImport :: BuildProvider -> Could not update item " + sitecoreProvider.ItemName + ": " + ex.ToString());
            }


            return sitecoreProvider;
        }
    }
}
