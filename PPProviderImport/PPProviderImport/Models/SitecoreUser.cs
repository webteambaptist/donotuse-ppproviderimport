﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPProviderImport.Models
{
    public class SitecoreUser
    {
        public string domain { get; set; }
        public string username { get; set; }
        public string password {get; set; }
    }
}
