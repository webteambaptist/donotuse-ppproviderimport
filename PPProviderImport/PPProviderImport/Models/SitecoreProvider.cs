﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PPProviderImport.Models
{
    public class SitecoreProvider
    {
        public string ParentID { get; set; }
        public string ItemName { get; set; }
        public string TemplateID { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string LastName { get; set; }
        public string Gender { get; set; }
        public string JobTitle { get; set; }
        public string Languages { get; set; }
        public string NPI { get; set; }
        public string PrimarySpecialty { get; set; }
        public string OtherSpecialties { get; set; }
        public string Suffix { get; set; }
        public string Biography { get; set; }
        public string PracticeInformation { get; set; }
        public string HospitalAffiliations { get; set; }
        public string IsAcceptingPatients { get; set; }
        public string AgesTreated { get; set; }
        public string IsBPP { get; set; }
        public string ExcludeFromSearch { get;set; }
        public string PrimaryLocation { get; set; }
        public string PrimaryLocationLatLng { get; set; }
        public string OtherLocations { get; set; }
        public string OtherLocationLatLngs { get; set; }
        public string Internship { get; set; }
        public string Residency { get; set; }
        public string CellPhoneNumber { get; set; }
        public string PageNumber {get;set;}
        public string PhoneNumber {get;set;}
        public string PhotoURL {get; set; }
    }
}
