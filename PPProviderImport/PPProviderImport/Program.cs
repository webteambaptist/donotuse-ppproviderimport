﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using PPProviderImport.Models;
using NLog;
namespace PPProviderImport
{
    public class Program
    {
        private static readonly Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        private static readonly string To = ConfigurationManager.AppSettings["recipient"];
        private static readonly string From = ConfigurationManager.AppSettings["sender"];
        private const string NewLine = "<br />";
        private static void Main(string[] args)
        {
            var config = new NLog.Config.LoggingConfiguration();
            var logFile = new NLog.Targets.FileTarget("logfile") {FileName = $"Logs\\PPProviderImport-{DateTime.Now:MM-dd-yyyy}.log"};
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            NLog.LogManager.Configuration = config;
            try
            {
                Console.WriteLine("Import started...");
                Logger.Info("Import started.....");
                Providers.GetProviderData();
                Console.WriteLine("Import complete!");
                Logger.Info("Import complete!");
                Environment.Exit(0);
            }
            catch (Exception ex)
            {
                Logger.Info("Program/Main()" + ex);
                var m = new Mail()
                {
                    From = From,
                    To = To,
                    Body = "Exception occurred while running Program.Main() " + NewLine + " ex.Message: " + ex.Message +
                           NewLine + " ex.InnerException: " +
                           ex.InnerException + NewLine + "Full Exception: " + ex,
                    Subject = "BHSitecorePP Process Failed"
                };
                var res = SendMail.SendMailMessage(m);
                if (res.StatusCode == HttpStatusCode.OK)
                {
                    Console.WriteLine("Error email sent successfully!");
                    Logger.Info("Error email sent successfully!");
                }
                else
                {
                    Console.WriteLine("Error sending Email");
                    Logger.Info("Error sending Email");
                }
                Console.WriteLine(ex.Message);
                Logger.Error("Program Main Exception: " + ex.ToString());
                Environment.Exit(0);
            }
        }
    }
}
