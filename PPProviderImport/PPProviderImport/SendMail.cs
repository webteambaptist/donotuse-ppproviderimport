﻿using Newtonsoft.Json;
using PPProviderImport.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace PPProviderImport
{
    public static class SendMail
    {
        private static readonly string BaseUrl = ConfigurationManager.AppSettings["mail"];
        public static HttpWebResponse SendMailMessage(Mail m)
        {
            HttpWebResponse webResponse = null;
            string jo = JsonConvert.SerializeObject(m).ToString();
            try
            {
                var httpWebRequest = (HttpWebRequest)WebRequest.Create(BaseUrl);

                httpWebRequest.ContentType = "application/json";
                httpWebRequest.Method = "POST";
                httpWebRequest.Timeout = 200000;
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(jo);
                    streamWriter.Flush();
                    streamWriter.Close();
                }
                webResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            }
            catch (WebException wex)
            {
                
            }
            catch (Exception ex)
            {
                
            }
            return webResponse;
        }
    }
}
